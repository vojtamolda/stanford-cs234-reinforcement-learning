# Stanford Course CS234 - Reinforcement Learning (Spring 2019)


Assignments, syllabus, solutions and other materials can be downloaded from the [course webpage](http://web.stanford.edu/class/cs234/index.html).
 - Schedule: [Stanford CS234](http://web.stanford.edu/class/cs234/schedule.html)
 - Assignments: [Stanford CS234](http://web.stanford.edu/class/cs234/assignments.html)
 - Final Project: [Stanford CS234](http://web.stanford.edu/class/cs234/project.html)
 
## Assignments
 - [ ] Assignment 1: Value and Policy Iteration [`assignment1/questions.pdf`](assignment1/questions.pdf)
 - [ ] Assignment 2: Q-Learning on Atari Games [`assignment2/readme.md`](assignment2/readme.md)
 - [ ] Assignment 3: Policy Gradient and Armed Bandit [`assignment3/questions.pdf`](assignment3/questions.pdf)


## Prerequisites

 - **Proficiency in Python** - All class assignments will be in Python (using numpy and Tensorflow and optionally Keras). There is a tutorial here for those who aren't as familiar with Python. If you have a lot of programming experience but in a different language (e.g. C/ C++/ Matlab/ Javascript) you will probably be fine.
 - **College Calculus, Linear Algebra** (e.g. MATH 51, CME 100) - You should be comfortable taking derivatives and understanding matrix vector operations and notation.
 - **Basic Probability and Statistics** (e.g. CS 109 or other stats course) - You should know basics of probabilities, Gaussian distributions, mean, standard deviation, etc.
 - **Foundations of Machine Learning** - We will be formulating cost functions, taking derivatives and performing optimization with gradient descent. Either CS 221 or CS 229 cover this background. Some optimization tricks will be more intuitive with some knowledge of convex optimization.


## Course Description

To realize the dreams and impact of AI requires autonomous systems that learn to make good decisions. Reinforcement learning is one powerful paradigm for doing so, and it is relevant to an enormous range of tasks, including robotics, game playing, consumer modeling and healthcare. This class will provide a solid introduction to the field of reinforcement learning and students will learn about the core challenges and approaches, including generalization and exploration. Through a combination of lectures, and written and coding assignments, students will become well versed in key ideas and techniques for RL.

Assignments will include the basics of reinforcement learning as well as deep reinforcement learning — an extremely promising new area that combines deep learning techniques with reinforcement learning. In addition, students will advance their understanding and the field of RL through a final project. 
